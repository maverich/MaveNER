FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
    python3-pip \
    python3

RUN pip3 install --upgrade pip

# Add and install Python modules
ADD requirements.txt /src/requirements.txt
RUN cd /src; pip3 install -r requirements.txt

# Bundle app source
ADD . /src

# Expose
EXPOSE  80

# Run
CMD ["python3", "/src/app.py"]
