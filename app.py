import tornado.web
import tornado.ioloop
import tornado.httpserver
import json
import time
import os

MAX_LENGTH = 10

def getAnswer(msg):
    if len(msg["msg"].split(" ")) > MAX_LENGTH:
        msg["ans"] = "TL;DR"
    else:
        msg["ans"] = time.strftime("I think you asked What time is it :D %d-%m-%y-%H-%M")
    return msg

class AnswerHandler(tornado.web.RequestHandler):
    def post(self):
        msg = {"msg":self.get_argument("msg")}
        self.write(json.dumps(getAnswer(msg=msg)))

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("ner.html")

class LoginPageHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("ner.html")

app = tornado.web.Application(handlers = [
(r"/", LoginPageHandler),
(r"/chatbot", MainHandler),
(r"/answer", AnswerHandler),
(r"/(.*)", tornado.web.StaticFileHandler, {
        "path": "static"
    })
])

http_server = tornado.httpserver.HTTPServer(app)
http_server.listen(80)
tornado.ioloop.IOLoop.instance().start()